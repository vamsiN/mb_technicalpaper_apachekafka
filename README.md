# MB_TechnicalPaper_ApacheKafka



## Introduction

Apache Kafka is an open source, distributed data streaming platform that allows for the development of real-time event-driven applications.  Kafka is distributed, It runs as a cluster that can span multiple servers or even multiple data centers.

Kafka mainly provides 3 key distributed funcitonalities

* To publish (write) and subscribe to (read) streams of events, including continuous import/export of your data from other systems.
* To store streams of events durably and reliably for as long as you want.
* To process streams of events as they occur or retrospectively.

Kafka allows to publish, subscribe to, store, and process streams of records in real time. It is designed to handle data streams delivers them to multiple consumers.

Specifically, it allows developers to make applications that continuously produce and consume streams of data records. 

The records that are produced are replicated and partitioned in such a way that allows for a high volume of users to use the application simultaneously without any perceptible lag in performance.

Apache Kafka is super fast. It also maintains a very high level of accuracy with the data records, and maintains the order of their occurrence, and is also resilient, fault-tolerant because it's replicated. So, these characteristics all together add up to an extremely powerful platform.

One great use case for Apache Kafka is decoupling system dependencies. So, with Apache Kafka, all the hard integrations go away and, instead, what we do is the checkout will stream events. 

If the developer wanted to make a retail application, for example, they would might make a checkout,
and then, with that checkout, when it happens, they want it to trigger a shipment. So, a user checks out and then the order gets shipped. They need to write an integration for that to happen, consider the shape of the data,  the way the data is transported, and the format of the data, but it's only one integration, so it's not a huge deal. But, as the application grows, maybe we want to add 
an automated email receipt when a checkout happens, or maybe we want to add an update to the inventory when a checkout happens. As front and back end services get added, and the application grows, more and more integrations need to get built and it can get very messy.

Not only that, but the teams in charge of each of the services are now reliant upon each other before they can make any changes and development is slow.

By using kafka, every time a checkout happens, that will get streamed, and the checkout is not concerned with who's listening to that stream. It's broadcasting those events.  Then the other services - email, shipment, inventory, they subscribe to that stream, they choose to listen to that one, and then they get the information they need and it triggers them to act accordingly. So, this is how Kafka can decouple your system dependencies and it is also a good use-case for how Kafka can be used for messaging, location tracking.

Kafka is built on five core APIs. The first one is the "producer" API. The producer API allows your application to produce, to make, these streams of data. So, it creates the records and produces them to topics. A "topic" is an ordered list of events. Now the topic can persist to disk that's where it can be saved for just a matter of minutes if it's going to be consumed immediately or you can have it saved for hours, days, or even forever. As long as you have enough storage space that the topics are persisted to physical storage.

Then we have the consumer API. The consumer API subscribes to one or more topics and listens and ingests that data. It can subscribe to topics in real time or it can consume those old data records that are saved to the topic. 

Now producers can produce directly to consumers  and that works for a simple Kafka application where the data doesn't change, but to transform that data, what we need is the streams API. The streams API is very powerful. It leverages the producer and the consumer APIs. So, it will consume from a topic or topics and then it will analyze, aggregate, or otherwise transform the data in real time, and then produce the resulting streams to a topic - either the same topics or to new topics. This is really at the core of what makes Kafka so amazing, and what powers the more complex use-cases like the location tracking or the data gathering. 

The Admin API to manage and inspect topics, brokers, and other Kafka objects.


Finally, we have the connector API. The connector API enables developers to write connectors, which are reusable producers and consumers. So, in a Kafka cluster many developers might need to integrate the same type of data source, like a MongoDB, for example. Well, not every single developer should have to write that integration, what the connector API allows is for that integration to get written once, the code is there, and then all the developer needs to do is configure it in order to get that data source into their cluster.



## Important commands to install and use kafka

Download the latest Kafka release and extract it:
```
$ tar -xzf kafka_2.13-3.2.0.tgz
$ cd kafka_2.13-3.2.0
```

starting the kafka environment
```bash
# Start the ZooKeeper service
# Note: Soon, ZooKeeper will no longer be required by Apache Kafka.

$ bin/zookeeper-server-start.sh config/zookeeper.properties
```
open another terminal session and run
```bash
# Start the Kafka broker service

$ bin/kafka-server-start.sh config/server.properties
```
Once all services have successfully launched, you will have a basic Kafka environment running and ready to use.

The documentation to create events and work with kafka is available at [documentation](https://kafka.apache.org/documentation/#design)

